<?php
/**
 * Your code here.
 *
 */

/* Setting up Automatic Update for All WordPress Plugins */
add_filter( 'auto_update_plugin', '__return_true' );

/* Setting up Automatic Update for All WordPress Plugins. Needed because WP won't do it when it finds a .git repo within the site */
add_filter( 'allow_dev_auto_core_updates', '__return_true' );           // Enable development updates 
add_filter( 'allow_minor_auto_core_updates', '__return_true' );         // Enable minor updates
add_filter( 'allow_major_auto_core_updates', '__return_true' );         // Enable major updates
## To enable automatic updates even if a VCS folder (.git, .hg, .svn etc) was found in the WordPress directory or any of its parent directories:
add_filter( 'automatic_updates_is_vcs_checkout', '__return_false', 1 );  
